import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCheckboxModule, MatSnackBarModule, MatInputModule,
  MatFormFieldModule, MatIconModule, MatCardModule, MatInput
} from '@angular/material';

// add material modules...
const modules = [
  MatButtonModule,
  MatSnackBarModule,
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatCardModule,
];
@NgModule({
  imports: modules,
  exports: modules,
})
export class CustomMaterialModule { }
