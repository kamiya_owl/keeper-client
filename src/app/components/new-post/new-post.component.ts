import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { KeeperService } from '../../services/keeper.service';
import { Post } from '../../services/post';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  @Output() add = new EventEmitter<Post>();

  text = '';
  constructor(
    public keeperService: KeeperService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  onSubmit(text: string) {
    this.keeperService
        .createPost(text)
        .subscribe(result => {
          this.text = '';
          this.add.emit(result);
        },
      err => {
        console.error(err);
        this.snackBar
            .open('登録に失敗しました', 'OK', { duration: 4000 });
      });
  }

}
