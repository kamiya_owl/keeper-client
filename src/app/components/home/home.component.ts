import { Component, OnInit } from '@angular/core';
import { KeeperService } from '../../services/keeper.service';
import { Observable } from 'rxjs/Observable';
import { Post } from '../../services/post';
import { AuthService } from '../../services/auth.service';
import { User } from '../../services/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  user: User;
  posts: Post[];
  constructor(
    public authService: AuthService,
    public keeperService: KeeperService,
  ) { }

  ngOnInit() {
    this.user = this.authService.getAuthUser();
    this.reload();
  }
  reload() {
    this.keeperService
    .getPosts()
    .subscribe(ps => this.posts = ps.reverse()); // 新しい順にしておく
  }

}
