import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;

  constructor(
    public router: Router,
    public authService: AuthService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      name: new FormControl('', [Validators.required]),
      pass: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]),
    });
  }
  onSubmit(valid: boolean, data: { email: string, name: string, pass: string }) {
    if (!valid) {
      return;
    }
    this.authService
      .register(data)
      .subscribe(user => {
        this.snackBar
          .open(`${user.name}さんの登録が完了しました`, 'OK', { duration: 4000 })
          .afterOpened()
          .subscribe(() => {
            this.router.navigate(['/login']);
          });

      }
        , err => {
          console.error(err);
          this.snackBar
            .open(`登録に失敗しました。${err.error}`, 'OK');
        });
  }
}
