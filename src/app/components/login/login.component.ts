import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    public router: Router,
    public authService: AuthService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    if (this.authService.isLogin()) {
      this.authService.logout();
    }
    this.form = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      pass: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]),
    });
  }
  onSubmit(valid: boolean, data: { email: string, name: string, pass: string }) {
    if (!valid) {
      return;
    }
    this.authService
      .authenticate(data)
      .subscribe(result => {
        this.router.navigate(['/home']);
      }
        , err => {
          console.error(err);
          this.snackBar
            .open(`ログインに失敗しました。${err.error}`, 'OK');
        });
  }
}
