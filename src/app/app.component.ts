import { Component, ViewEncapsulation } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from './app.config';
import { AuthService } from './services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,

    public authService: AuthService,
    public snackBar: MatSnackBar,
  ) {
    this.checkElectron();
    // apiの生存確認
    this.checkKeeper();
  }
  checkKeeper() {
    this.authService
      .ping()
      .subscribe(result => {
        console.log('ping', result);
      },
        err => {
          console.error(err);
          this.snackBar
            .open('APIサーバから応答がありません。サービスは利用できません');
        });
  }
  checkElectron() {
    this.translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (this.electronService.isElectron()) {
      console.log('Mode electron');
      console.log('Electron ipcRenderer', this.electronService.ipcRenderer);
      console.log('NodeJS childProcess', this.electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }
}
