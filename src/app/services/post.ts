export class Post {
    constructor(
        public ID: { $oid: string },
        public By: string, // user id
        public Data: string,
        public At: { $date: Date },
    ) { }
}
