import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Post } from './post';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class KeeperService {
  baseUrl = '/api/1.0/';
  // LocalStorageに保管されたtokenをヘッダにつける
  tokenGetter = () => localStorage.getItem('keeper-token');

  constructor(
    public http: HttpClient,
  ) { }

  private getAuthHeaders(headers: HttpHeaders = new HttpHeaders()): HttpHeaders {
    const token = this.tokenGetter();
    if (token) {
      return headers.append('Authorization', `Bearer ${token}`);
    }
    return headers;
  }
  // todo: paging, filter, etc...
  getPosts(): Observable<Post[]> {
    return this.http
               .get<Post[]>(
                 this.baseUrl + 'post',
                 { headers: this.getAuthHeaders() }
               );
  }
  createPost(text: string): Observable<Post> {
    return this.http.post<Post>(
      this.baseUrl + 'post',
      { data: text },
      { headers: this.getAuthHeaders() }
    );
  }
  // TODO: update, delete
}
