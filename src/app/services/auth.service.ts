import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from './user';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthService {
  baseUrl = '/api/1.0/';

  constructor(
    public http: HttpClient,
  ) { }

  ping(): Observable<string> {
    return this.http.get(this.baseUrl, { responseType: 'text' });
  }
  register(
    data: { email: string, pass: string, name: string },
  ): Observable<User> {
    return this.http.post<User>(
      this.baseUrl + 'register', data);
  }
  // アカウント認証、成功した場合localStorageにトークンを自動セットしてくれる
  authenticate(
    data: { email: string, pass: string },
  ): Observable<{ token: string, user: User }> {
    return this.http
      .post<{ token: string, user: User }>(
        this.baseUrl + 'authenticate', data
      )
      .do(res => {
        // 成功した場合は'keeper-token'にトークンをつけてあげる
        localStorage.setItem('keeper-token', res.token);
        localStorage.setItem('keeper-user', JSON.stringify(res.user));
      });
  }
  // ログインユーザの取得、いなければnull
  getAuthUser(): User {
    const userJson = localStorage.getItem('keeper-user');
    if (!userJson) {
      return null;
    }
    let user: User;
    try {
      user = JSON.parse(userJson);
      if (!user._id) {
        return null;
      }
    } catch (ex) {
      console.log(ex);
      return null;
    }
    return user;
  }
  // ログイン状態の取得
  isLogin() {
    const token: string = localStorage.getItem('keeper-token');
    const userJson = localStorage.getItem('keeper-user');
    if (!token || !userJson) {
      return false;
    }
    let user: User;
    try {
      user = JSON.parse(userJson);
      if (!user._id) {
        return false;
      }
    } catch (ex) {
      console.log(ex);
      return false;
    }
    return true;
  }
  // ローカル保持した情報を消しておく
  logout() {
    localStorage.removeItem('keeper-token');
    localStorage.removeItem('keeper-user');
  }
}
