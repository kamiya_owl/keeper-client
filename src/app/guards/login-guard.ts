import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(
        public router: Router,
        public authService: AuthService,

        public snackBar: MatSnackBar,
    ) { }
    canActivate() {
        const result = this.authService.isLogin();
        if (!result) {
            this.router.navigate(['login']);
            this.snackBar.open('利用にはログインが必要です', 'OK', { duration: 4000 });
        }
        return result;
    }
}

